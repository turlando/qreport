#ifndef CONSTS_H
#define CONSTS_H

#define APPLICATION_NAME "QReport"
#define ORGANIZATION_NAME "CoseLosche Inc."
#define APPLICATION_VERSION "0.3"

enum { MaxRecentFiles = 5,
       ColumnsBeforeRowSplit = 7 };
enum TableAction { Add = 0,
                   Remove = 1 };
enum Position { Top    = 1,
                Bottom = 2,
                Above  = 3,
                Below  = 4 };
enum RemoveAct { Selected = 0,
                 All      = 1 };
enum document_type_t { DIABETICS = 0,
                       DISABLED  = 1 };

#endif // CONSTS_H
