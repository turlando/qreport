#include "documenthandler.h"

#include <QJsonParseError>

DocumentHandler::DocumentHandler() {}

DocumentHandler::~DocumentHandler() {}

bool DocumentHandler::open(const QString &fileName) {
  this->setFileName(fileName);
  if (!file.open(QIODevice::ReadOnly))
    return false;
  QJsonParseError e;
  d = QJsonDocument::fromJson(file.readAll(), &e).object();
  file.close();
  return true;
}

QString DocumentHandler::fileName() {
  return file.fileName();
}

void DocumentHandler::setFileName(const QString &fileName) {
  file.setFileName(fileName);
}

bool DocumentHandler::isLoaded() {
  if (file.fileName() == QString::null)
    return false;
  return true;
}

void DocumentHandler::close() {
  file.setFileName(QString::null);
  d = QJsonObject();
}

bool DocumentHandler::save() {
  if (!this->isLoaded())
    return false;
  if (!file.open(QIODevice::WriteOnly))
    return false;
  QByteArray serialized = QJsonDocument(d).toJson();
  file.write(serialized, serialized.size());
  file.close();
  return true;
}

QVariantMap DocumentHandler::map() {
  return d.toVariantMap();
}

void DocumentHandler::set(QString entry, QString value) {
  d[entry] = value;
}

document_type_t DocumentHandler::type() {
  switch(d["type"].toInt()) {
    case 0:
      return DIABETICS; break;
    case 1:
      return DISABLED; break;
    default:
      return DIABETICS; break;
  }
}

void DocumentHandler::setType(document_type_t type) {
  switch(type) {
    case DIABETICS:
      d["type"] = 0; break;
    case DISABLED:
      d["type"] = 1; break;
  }
}

unsigned int DocumentHandler::year() {
  return d["year"].toInt();
}

void DocumentHandler::setYear(int year) {
  d["year"] = year;
}

unsigned int DocumentHandler::month() {
  return d["month"].toInt();
}

void DocumentHandler::setMonth(int month) {
  d["month"] = month;
}

QString DocumentHandler::storeName() {
  return d["storeName"].toString();
}

void DocumentHandler::setStoreName(QString name) {
  d["storeName"] = name;
}

unsigned int DocumentHandler::storeCode() {
  return d["storeCode"].toInt();
}

void DocumentHandler::setStoreCode(int code) {
  d["storeCode"] = code;
}

QString DocumentHandler::address() {
  return d["address"].toString();
}

void DocumentHandler::setAddress(QString address) {
  d["address"] = address;
}

QString DocumentHandler::uuid() {
  return d["uuid"].toString();
}

void DocumentHandler::setUuid(QString uuid) {
  d["uuid"] = uuid;
}

QString DocumentHandler::iva() {
  return d["iva"].toString();
}

void DocumentHandler::setIva(QString iva) {
  d["iva"] = iva;
}

QJsonArray DocumentHandler::entries() {
  return d["entries"].toArray();
}

void DocumentHandler::setEntries(QJsonArray entries) {
  d["entries"] = entries;
}
