#ifndef HTMLSOUP_H
#define HTMLSOUP_H

#include <QVariantMap>

class HtmlSoup {
  public:
    HtmlSoup(const QVariantMap d);
    QString document();

  private:
    bool loadTemplate(const QString fileName);
    void fill(const QString placeHolder, const QString value);
    QString documentFileName();
    QString documentPath();
    QString thSurround(QString s);
    QString tdSurround(QString s);
    QString generateTable();

    QString html;

  private:
    QVariantMap _d;
};

#endif // HTMLSOUP_H
