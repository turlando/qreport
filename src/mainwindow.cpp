#include "mainwindow.h"
#include "htmlsoup.h"
#include <QApplication>
#include <QSettings>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QDesktopServices>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QGroupBox>
#include <QSpinBox>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QTableWidget>

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent) {
  createActions();
  createMenus();
  updateRecentFilesActions();
  createStatusBar();

  QVBoxLayout *layout = new QVBoxLayout;
  QGroupBox *headingGroupBox = new QGroupBox(tr("Heading"));
  QGroupBox *entriesGroupBox = new QGroupBox(tr("Entries"));
  layout->addWidget(headingGroupBox);
  layout->addWidget(entriesGroupBox);

  QVBoxLayout *headingLayout = new QVBoxLayout;

  QHBoxLayout *headingRow1 = new QHBoxLayout;
  QFormLayout *storeNameFormLayout = new QFormLayout;
  storeNameLineEdit = new QLineEdit;
  storeNameFormLayout->addRow(tr("Pharmacy name:"), storeNameLineEdit);
  QFormLayout *storeCodeFormLayout = new QFormLayout;
  storeCodeSpinBox = new QSpinBox;
  storeCodeSpinBox->setMinimum(0); storeCodeSpinBox->setMaximum(999);
  storeCodeFormLayout->addRow(tr("Pharmacy code:"), storeCodeSpinBox);
  headingRow1->addLayout(storeNameFormLayout); headingRow1->addSpacing(10);
  headingRow1->addLayout(storeCodeFormLayout);

  QHBoxLayout *headingRow2 = new QHBoxLayout;
  QFormLayout *addressFormLayout = new QFormLayout;
  addressLineEdit = new QLineEdit;
  addressFormLayout->addRow(tr("Address:"), addressLineEdit);
  headingRow2->addLayout(addressFormLayout);

  QHBoxLayout *headingRow3 = new QHBoxLayout;
  QFormLayout *uuidFormLayout = new QFormLayout;
  uuidLineEdit = new QLineEdit;
  uuidLineEdit->setInputMask(">NNNNNNNNNNNNNNNN");
  uuidFormLayout->addRow(tr("C.F.:"), uuidLineEdit);
  QFormLayout *ivaFormLayout = new QFormLayout;
  ivaLineEdit = new QLineEdit;
  ivaLineEdit->setInputMask("999999999999");
  ivaFormLayout->addRow(tr("IVA:"), ivaLineEdit);
  headingRow3->addLayout(uuidFormLayout); headingRow3->addSpacing(10);
  headingRow3->addLayout(ivaFormLayout);

  QHBoxLayout *headingRow4 = new QHBoxLayout;
  QFormLayout *typeFormLayout = new QFormLayout;
  typeComboBox = new QComboBox;
  typeComboBox->addItem(tr("Diabetics"));
  typeComboBox->addItem(tr("Disabled"));
  connect(typeComboBox, SIGNAL(currentIndexChanged(int)),
          this, SLOT(onTypeComboBoxIndexChanged(int)));
  typeFormLayout->addRow(tr("Document type:"), typeComboBox);
  QFormLayout *dateFormLayout = new QFormLayout;
  dateEdit = new QDateTimeEdit(QDate::currentDate());
  dateEdit->setDisplayFormat("yyyy/MM");
  dateFormLayout->addRow(tr("Date:"), dateEdit);
  headingRow4->addLayout(typeFormLayout); headingRow4->addSpacing(10);
  headingRow4->addLayout(dateFormLayout);

  headingLayout->addLayout(headingRow1);
  headingLayout->addLayout(headingRow2);
  headingLayout->addLayout(headingRow3);
  headingLayout->addLayout(headingRow4);
  headingGroupBox->setLayout(headingLayout);

  QHBoxLayout *entriesLayout = new QHBoxLayout;

  entriesTable = new QTableWidget(0, 1, this);
  entriesTable->setHorizontalHeaderLabels(QStringList(tr("Value")));
  entriesTable->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(entriesTable, SIGNAL(customContextMenuRequested(QPoint)),
          this, SLOT(onTableCustomMenuRequested(QPoint)));
  connect(entriesTable, SIGNAL(itemChanged(QTableWidgetItem*)),
          this, SLOT(onTableItemChanged(QTableWidgetItem*)));

  QFormLayout *previewFormLayout = new QFormLayout;
  previewFormLayout->setFormAlignment(Qt::AlignVCenter);
  previewEntriesLineEdit = new QLineEdit;
  previewEntriesLineEdit->setReadOnly(true);
  previewEntriesLineEdit->setText("0");
  previewFormLayout->addRow(tr("Entries:"), previewEntriesLineEdit);
  previewTotalLineEdit = new QLineEdit;
  previewTotalLineEdit->setReadOnly(true);
  previewTotalLineEdit->setText("€ 0.00");
  previewFormLayout->addRow(tr("Total:"), previewTotalLineEdit);
  previewIvaLineEdit = new QLineEdit;
  previewIvaLineEdit->setReadOnly(true);
  previewIvaLineEdit->setText("€ 0.00");
  previewFormLayout->addRow(tr("IVA:"), previewIvaLineEdit);
  previewTaxableLineEdit = new QLineEdit;
  previewTaxableLineEdit->setReadOnly(true);
  previewTaxableLineEdit->setText("€ 0.00");
  previewFormLayout->addRow(tr("Taxable total:"), previewTaxableLineEdit);

  entriesLayout->addWidget(entriesTable);
  entriesLayout->addSpacing(10);
  entriesLayout->addLayout(previewFormLayout);
  entriesGroupBox->setLayout(entriesLayout);

  setWindowTitle(APPLICATION_NAME);
  QWidget *window = new QWidget;
  window->setLayout(layout);
  setCentralWidget(window);
}

MainWindow::~MainWindow() {}

void MainWindow::createActions() {
  newAct = new QAction(tr("&New"), this);
  newAct->setShortcuts(QKeySequence::New);
  newAct->setStatusTip(tr("Create a new document"));
  connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

  openAct = new QAction(tr("&Open..."), this);
  openAct->setShortcuts(QKeySequence::Open);
  openAct->setStatusTip(tr("Open an existing document"));
  connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

  saveAct = new QAction(tr("&Save"), this);
  saveAct->setShortcut(QKeySequence::Save);
  saveAct->setStatusTip(tr("Save the document to disk"));
  connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

  saveAsAct = new QAction(tr("Save &As..."), this);
  saveAsAct->setShortcuts(QKeySequence::SaveAs);
  saveAsAct->setStatusTip(tr("Save the document under a new name"));
  connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

  for (int i = 0; i < MaxRecentFiles; ++i) {
    recentFilesAct[i] = new QAction(this);
    recentFilesAct[i]->setVisible(false);
    connect(recentFilesAct[i], SIGNAL(triggered()),
            this, SLOT(openRecentFile()));
  }

  exportAct = new QAction(tr("&Export document..."), this);
  //TODO:shortcut
  exportAct->setStatusTip(tr("Export document"));
  connect(exportAct, SIGNAL(triggered()), this, SLOT(exportDocument()));

  exitAct = new QAction(tr("E&xit"), this);
  exitAct->setShortcuts(QKeySequence::Quit);
  exitAct->setStatusTip(tr("Quit the application"));
  connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));

  aboutAct = new QAction(tr("&About"), this);
  aboutAct->setStatusTip(tr("About this software"));
  connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
}

void MainWindow::createMenus() {
  recentsMenu = new QMenu(tr("Recent &documents"), this);
  for (int i = 0; i < MaxRecentFiles; ++i)
    recentsMenu->addAction(recentFilesAct[i]);

  fileMenu = menuBar()->addMenu(tr("&File"));
  fileMenu->addAction(newAct);
  fileMenu->addAction(openAct);
  fileMenu->addAction(saveAct);
  fileMenu->addAction(saveAsAct);
  fileMenu->addMenu(recentsMenu);
  fileMenu->addAction(exportAct);
  fileMenu->addSeparator();
  fileMenu->addAction(exitAct);

  helpMenu = menuBar()->addMenu(tr("&Help"));
  helpMenu->addAction(aboutAct);
}

void MainWindow::createStatusBar() {
  statusBar()->showMessage(tr("Ready"));
}

void MainWindow::newFile() {
  MainWindow *other = new MainWindow;
  other->show();
}

void MainWindow::open() {
  QString fileName = QFileDialog::getOpenFileName(this, tr("Open document"),
                                                  QDir::homePath(),
                                                  tr("Reports (*.json);;\
                                                     All files (*.*)"));
  if (!fileName.isEmpty()) {
    if (QFileInfo(fileName).completeSuffix() == "json")
      loadFile(fileName);
    else
      QMessageBox::critical(this, tr("Error"),
                            tr("Document not valid."));
  }
}

void MainWindow::openRecentFile() {
  QAction *action = qobject_cast<QAction *>(sender());
  if (action)
    loadFile(action->data().toString());
}

void MainWindow::save() {
  if (!d.isLoaded())
    return saveAs();
  saveFile(d.fileName());
}

void MainWindow::saveAs() {
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save document"),
                                                  QDir::homePath(),
                                                  tr("Reports (*.json)"));
  if (fileName.isEmpty())
    return;
  return saveFile(fileName);
}

void MainWindow::exportDocument() {
  collectData();
  HtmlSoup soup(d.map());
  QDesktopServices::openUrl(QUrl(soup.document()));
}

void MainWindow::about() {
  QMessageBox::about(this, tr("About"),
                     QString("%1 v%2\n%3")
                     .arg(QCoreApplication::applicationName())
                     .arg(QCoreApplication::applicationVersion())
                     .arg(QCoreApplication::organizationName()));
}

void MainWindow::onTypeComboBoxIndexChanged(int index) {
  updatePreviewForm();
}

void MainWindow::onTableItemChanged(QTableWidgetItem *item) {
  QString text = item->text();
  text.trimmed();
  text.replace(",", ".");
  if (!text.contains("."))
    text.append(".00");
  if (text.split(".")[1].size() == 1)
    text.append("0");
  item->setText(text);
  updatePreviewForm();
}

void MainWindow::onTableCustomMenuRequested(QPoint point) {
  cMenu = new QMenu(this);
  QVariantMap data = QVariantMap();
  data.insert("point", point);
  data.insert("position", 0);

  data.insert("action", 0);
  addTopAct = new QAction(tr("Add row at top"), cMenu);
  data.insert("position", 1);
  addTopAct->setData(QVariant(data));
  addBottomAct = new QAction(tr("Add row at bottom"), cMenu);
  data.insert("position", 2);
  addBottomAct->setData(QVariant(data));
  addAboveAct = new QAction(tr("Add row above"), cMenu);
  data.insert("position", 3);
  addAboveAct->setData(QVariant(data));
  addBelowAct = new QAction(tr("Add row below"), cMenu);
  data.insert("position", 4);
  addBelowAct->setData(QVariant(data));

  data.remove("position");

  data.insert("action", 1);
  removeAct = new QAction(tr("Remove selected row"), cMenu);
  data.insert("selection", 0);
  removeAct->setData(data);
  removeAllAct = new QAction(tr("Remove all rows"), cMenu);
  data.insert("selection", 1);
  removeAllAct->setData(data);

  cMenu->addAction(addTopAct);
  cMenu->addAction(addBottomAct);
  cMenu->addSeparator();
  cMenu->addAction(addAboveAct);
  cMenu->addAction(addBelowAct);
  cMenu->addSeparator();
  cMenu->addAction(removeAct);
  cMenu->addAction(removeAllAct);

  connect(cMenu, SIGNAL(triggered(QAction*)), this,
          SLOT(manageTableItem(QAction*)));
  cMenu->popup(entriesTable->viewport()->mapToGlobal(point));
}

void MainWindow::manageTableItem(QAction *action) {
  QVariantMap data = action->data().toMap();
  TableAction tAct = TableAction(data.take("action").toInt());
  QPoint point = data.take("point").toPoint();
  Position position = Position(data.take("position").toInt());
  QTableWidgetItem *item = entriesTable->itemAt(point);
  switch (tAct) {
    case Add:
      switch (position) {
        case Top:
          entriesTable->insertRow(0);
          break;
        case Bottom:
          entriesTable->insertRow(entriesTable->rowCount());
          break;
        case Above: {
          QTableWidgetItem *item = entriesTable->itemAt(point);
          if (item)
            entriesTable->insertRow(item->row());
          break;
        }
        case Below: {
          QTableWidgetItem *item = entriesTable->itemAt(point);
          if (item)
            entriesTable->insertRow(item->row() + 1);
          break;
        }
        default:
          break;
      }
      break;
    case Remove:
      RemoveAct rAct = RemoveAct(data.take("selection").toInt());
      switch (rAct) {
        case Selected:
          if (item)
            entriesTable->removeRow(item->row());
          break;
        case All:
          while (entriesTable->rowCount() > 0)
            entriesTable->removeRow(0);
          break;
      }
      updatePreviewForm();
      break;
  }
}

void MainWindow::loadFile(const QString &fileName) {
  d.open(fileName);
  MainWindow::addRecent(fileName);
  setForms();
  statusBar()->showMessage(tr("Document loaded: %1").arg(fileName), 2000);
}

void MainWindow::saveFile(const QString &fileName) {
  d.setFileName(fileName);
  collectData();
  d.save();
  statusBar()->showMessage(tr("Document saved: %1").arg(fileName), 2000);
}

void MainWindow::addRecent(const QString &fileName) {
  QSettings settings;
  QStringList files = settings.value("RecentFilesList").toStringList();
  files.removeAll(fileName);
  files.prepend(fileName);
  while (files.size() > MaxRecentFiles)
    files.removeLast();
  settings.setValue("RecentFilesList", files);
  foreach (QWidget *widget, QApplication::topLevelWidgets()) {
    MainWindow *mainWin = qobject_cast<MainWindow *>(widget);
    if (mainWin)
      mainWin->updateRecentFilesActions();
  }
}

void MainWindow::updateRecentFilesActions() {
  QSettings settings;
  QStringList files = settings.value("RecentFilesList").toStringList();
  int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);
  for (int i = 0; i < numRecentFiles; ++i) {
    QString text = tr("&%1 %2").arg(i + 1).arg(getFileName(files[i]));
    recentFilesAct[i]->setText(text);
    recentFilesAct[i]->setData(files[i]);
    recentFilesAct[i]->setVisible(true);
  }
  for (int i = numRecentFiles; i < MaxRecentFiles; ++i)
    recentFilesAct[i]->setVisible(false);
}

QString MainWindow::getFileName(const QString &fullPath) {
  return QFileInfo(fullPath).fileName();
}

int MainWindow::tableEntries() {
  return entriesTable->rowCount();
}

double MainWindow::tableTotal() {
  double total = 0;
  for (int i = 0; i < entriesTable->rowCount(); i++) {
    QTableWidgetItem *item = entriesTable->item(i, 0);
    if (item)
      total += item->text().toDouble();
  }
  return total;
}

double MainWindow::tableIva() {
  switch (typeComboBox->currentIndex()) {
    case 0:
      return tableTotal()*0.04;
    case 1:
      return tableTotal()*0.10;
    default:
      return (double) 0.00;
  }
}

double MainWindow::tableTaxable() {
  return tableTotal()-tableIva();
}

void MainWindow::updatePreviewForm() {
  previewEntriesLineEdit->setText(QString("%1").setNum(tableEntries()));
  previewTotalLineEdit->setText(QString("€ %1").setNum(tableTotal(), 'f', 2));
  previewIvaLineEdit->setText(QString("€ %1").setNum(tableIva(), 'f', 2));
  previewTaxableLineEdit->setText(QString("€ %1").setNum(tableTaxable(), 'f',
                                                         2));
}

void MainWindow::setForms() {
  storeNameLineEdit->setText(d.storeName());
  storeCodeSpinBox->setValue(d.storeCode());
  addressLineEdit->setText(d.address());
  uuidLineEdit->setText(d.uuid());
  ivaLineEdit->setText(d.iva());
  typeComboBox->setCurrentIndex(d.type());
  dateEdit->setDate(QDate(d.year(), d.month(), 1));
  entriesTable->setRowCount(d.entries().size());
  for (int i = 0; i <= d.entries().size(); i++)
    entriesTable->setItem(i, 0,
        new QTableWidgetItem(QString::number(d.entries().at(i).toDouble())));
}

void MainWindow::collectData() {
  d.setStoreName(storeNameLineEdit->text());
  d.setStoreCode(storeCodeSpinBox->text().toInt());
  d.setAddress(addressLineEdit->text());
  d.setUuid(uuidLineEdit->text());
  d.setIva(ivaLineEdit->text());
  d.setType(document_type_t(typeComboBox->currentIndex()));
  d.setYear(dateEdit->date().year());
  d.setMonth(dateEdit->date().month());
  QJsonArray entries = d.entries();
  while(!entries.empty())
    entries.removeFirst(); //@TODO: is there a better way?
  for(int i = 0; i < entriesTable->rowCount(); i++)
    entries.append(QJsonValue(entriesTable->item(i, 0)->text()));
  d.setEntries(entries);
  d.set("entriesCount", previewEntriesLineEdit->text());
  d.set("summaryTotal", previewTotalLineEdit->text());
  d.set("summaryIva", previewIvaLineEdit->text());
  d.set("summaryTaxable", previewTaxableLineEdit->text());
}
