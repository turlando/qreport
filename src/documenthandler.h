#ifndef DOCUMENTHANDLER_H
#define DOCUMENTHANDLER_H

#include "consts.h"
#include <QVariantMap>
#include <QJsonArray>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

class DocumentHandler {
  public:
    DocumentHandler();
    ~DocumentHandler();

    bool open(const QString &fileName);
    QString fileName();
    void setFileName(const QString &fileName);
    bool isLoaded();
    void close();
    bool save();

    QVariantMap map();
    void set(QString entry, QString value);
    unsigned int version();
    void setVersion(int version);
    document_type_t type();
    void setType(document_type_t type);
    unsigned int year();
    void setYear(int year);
    unsigned int month();
    void setMonth(int month);
    QString storeName();
    void setStoreName(QString name);
    unsigned int storeCode();
    void setStoreCode(int code);
    QString address();
    void setAddress(QString address);
    QString uuid();
    void setUuid(QString uuid);
    QString iva();
    void setIva(QString iva);
    QJsonArray entries();
    void setEntries(QJsonArray entries);

  private:
    QFile file;
    QJsonObject d;
};

#endif // DOCUMENTHANDLER_H
