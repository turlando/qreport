﻿#include "htmlsoup.h"
#include "consts.h"
#include <QDir>
#include <QTextStream>

HtmlSoup::HtmlSoup(const QVariantMap d) {
  _d = d;
}

bool HtmlSoup::loadTemplate(const QString fileName) {
  QFile file(fileName);
  if (!file.open(QIODevice::ReadOnly))
    return false;
  html = QString(file.readAll());
  file.close();
  return true;
}

void HtmlSoup::fill(const QString placeHolder, const QString value) {
  html.replace(placeHolder, value);
}

QString HtmlSoup::documentFileName() {
  QString filename = "document-";
  switch(_d["type"].toInt()) {
    case 0:
      filename.append("diabetics"); break;
    case 1:
      filename.append("disabled"); break;
    default:
      filename.append("diabetics"); break;
  }
  filename.append("-" +_d["year"].toString() + "-" + _d["month"].toString());
  filename.append(".html");
  return filename;
}

QString HtmlSoup::documentPath() {
  return QDir::tempPath() + "/" + documentFileName();
}

QString HtmlSoup::tdSurround(QString s) {
  return s.prepend("<td>").append("</td>");
}

QString HtmlSoup::thSurround(QString s) {
  return s.prepend("<th>").append("</th>");
}

QString HtmlSoup::generateTable() {
  QString table;
  QVariantList entries = _d["entries"].toList();
  int entriesSize = entries.size();
  int rowsSize = entriesSize / ColumnsBeforeRowSplit + 1;
  int columnsSize = entriesSize / rowsSize + 1;
  table += "<thead><tr>";
  for(int i = 0; i < columnsSize; i++) {
    table += thSurround("n&deg;");
    table += thSurround("importo");
  }
  table += "</tr></thead>";
  table += "<tbody>";
  bool endOfData = false;
  bool printedAnyColumns = false;
  for(int row = 0; row < rowsSize; row++) {
    for(int col = 0; col < columnsSize; col++) {
      endOfData = col * rowsSize + row >= entriesSize;
      if(endOfData) {
        if(!printedAnyColumns)
          break;
      }
      if(!printedAnyColumns)
        table += "<tr>";
      printedAnyColumns = true;
      if(!endOfData) {
        table += tdSurround(QString::number(col * rowsSize + row + 1));
        table += tdSurround(entries[col * rowsSize + row].toString());
      } else {
        table += tdSurround("") + tdSurround("");
      }
    }
    if(printedAnyColumns)
      table += "</tr>";
  }
  table += "</tbody>";
  return table;
}

QString HtmlSoup::document() {
  QFile file;
  switch(_d["type"].toInt()) {
    case 0:
      loadTemplate("res/diabetics.html"); break;
    case 1:
      loadTemplate("res/disabled.html"); break;
    default:
      loadTemplate("res/diabetics.html"); break;
  }

  file.setFileName("res/reset.css");
  if (file.open(QIODevice::ReadOnly)) {
    fill("${RES_CSS_RESET}", file.readAll());
    file.close();
  }

  file.setFileName("res/style.css");
  if (file.open(QIODevice::ReadOnly)) {
    fill("${RES_CSS_STYLE}", file.readAll());
    file.close();
  }

  fill("${FILENAME}", documentFileName());
  fill("${STORE_NAME}", _d["storeName"].toString());
  fill("${STORE_CODE}", _d["storeCode"].toString());
  fill("${STORE_ADDRESS}", _d["address"].toString());
  fill("${UUID}", _d["uuid"].toString());
  fill("${IVA}", _d["iva"].toString());
  fill("${IVA}", _d["iva"].toString());
  fill("${YEAR}", _d["year"].toString());
  fill("${MONTH}", _d["month"].toString());
  fill("${ENTRIES_COUNT}", _d["entriesCount"].toString());
  fill("${SUMMARY_TAXABLE}", _d["summaryTaxable"].toString());
  fill("${SUMMARY_IVA}", _d["summaryIva"].toString());
  fill("${SUMMARY_TOTAL}", _d["summaryTotal"].toString());
  fill("${ENTRIES_TABLE}", generateTable());

  file.setFileName(documentPath());
  if (!file.open(QIODevice::WriteOnly))
    return QString();
  QTextStream out(&file);
  out.setCodec("UTF-8");
  out << html;
  file.close();
  return HtmlSoup::documentPath();
}
