#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "consts.h"
#include "documenthandler.h"
#include <QMainWindow>
#include <QTableWidgetItem>
#include <QSpinBox>
#include <QComboBox>
#include <QDateTimeEdit>

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

  private:
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *recentFilesAct[MaxRecentFiles];
    QAction *exportAct;
    QAction *exitAct;
    QAction *aboutAct;

    QAction *addTopAct;
    QAction *addBottomAct;
    QAction *addAboveAct;
    QAction *addBelowAct;
    QAction *removeAct;
    QAction *removeAllAct;

    QMenu *fileMenu;
    QMenu *recentsMenu;
    QMenu *helpMenu;

    QMenu *cMenu;

    void createActions();
    void createMenus();
    void createStatusBar();

    QLineEdit *storeNameLineEdit;
    QSpinBox *storeCodeSpinBox;
    QLineEdit *addressLineEdit;
    QLineEdit *uuidLineEdit;
    QLineEdit *ivaLineEdit;
    QDateTimeEdit *dateEdit;
    QTableWidget *entriesTable;
    QComboBox *typeComboBox;
    QLineEdit *previewEntriesLineEdit;
    QLineEdit *previewTotalLineEdit;
    QLineEdit *previewIvaLineEdit;
    QLineEdit *previewTaxableLineEdit;

    void loadFile(const QString &fileName);
    void saveFile(const QString &fileName);
    void addRecent(const QString &fileName);
    void updateRecentFilesActions();
    QString getFileName(const QString &fullPath);

    int tableEntries();
    double tableTotal();
    double tableIva();
    double tableTaxable();
    void updatePreviewForm();

    DocumentHandler d;

  private slots:
    void newFile();
    void open();
    void openRecentFile();
    void save();
    void saveAs();
    void exportDocument();
    void about();

    void onTypeComboBoxIndexChanged(int index);
    void onTableItemChanged(QTableWidgetItem *item);
    void onTableCustomMenuRequested(QPoint point);
    void manageTableItem(QAction *action);

    void setForms();
    void collectData();
};

#endif // MAINWINDOW_H
