#-------------------------------------------------
#
# Project created by QtCreator 2014-09-24T14:11:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QReport
TEMPLATE = app


SOURCES += src/main.cpp\
    src/mainwindow.cpp \
    src/documenthandler.cpp \
    src/htmlsoup.cpp

HEADERS  += src/mainwindow.h \
    src/consts.h \
    src/documenthandler.h \
    src/htmlsoup.h

HTML_RESOURCES = res/reset.css \
    res/style.css \
    res/diabetics.html \
    res/disabled.html

TRANSLATIONS += loc/qreport_it_IT.ts


updatehtmlres.input = HTML_RESOURCES
updatehtmlres.output = res/${QMAKE_FILE_BASE}${QMAKE_FILE_EXT}
updatehtmlres.commands = $(COPY_FILE) ${QMAKE_FILE_IN} ${QMAKE_FILE_OUT}
updatehtmlres.CONFIG += no_link

isEmpty(QMAKE_LRELEASE) {
  win32|os2:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]\lrelease.exe
  else:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]/lrelease
  unix {
    !exists($$QMAKE_LRELEASE) { QMAKE_LRELEASE = lrelease-qt }
  } else {
    !exists($$QMAKE_LRELEASE) { QMAKE_LRELEASE = lrelease }
  }
}

updateqm.input = TRANSLATIONS
updateqm.output = qm/${QMAKE_FILE_BASE}.qm
updateqm.commands = $$QMAKE_LRELEASE -silent ${QMAKE_FILE_IN} -qm ${QMAKE_FILE_OUT}
updateqm.CONFIG += no_link

PRE_TARGETDEPS += compiler_updatehtmlres_make_all \
    compiler_updateqm_make_all

QMAKE_EXTRA_COMPILERS += updatehtmlres \
    updateqm
