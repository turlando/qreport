<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="29"/>
        <source>Heading</source>
        <translation>Intestazione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="30"/>
        <source>Entries</source>
        <translation>Importi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="39"/>
        <source>Pharmacy name:</source>
        <translation>Nome farmacia:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="43"/>
        <source>Pharmacy code:</source>
        <translation>Codice farmacia:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="50"/>
        <source>Address:</source>
        <translation>Indirizzo:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="57"/>
        <source>C.F.:</source>
        <translation>C.F.:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="61"/>
        <location filename="../src/mainwindow.cpp" line="109"/>
        <source>IVA:</source>
        <translation>IVA:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="68"/>
        <source>Diabetics</source>
        <translation>Diabetici</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="69"/>
        <source>Disabled</source>
        <translation>Invalidi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="72"/>
        <source>Document type:</source>
        <translation>Tipologia distinta:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="76"/>
        <source>Date:</source>
        <translation>Data:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="89"/>
        <source>Value</source>
        <translation>Importo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="101"/>
        <source>Entries:</source>
        <translation>Importi:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="105"/>
        <source>Total:</source>
        <translation>Totale:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="113"/>
        <source>Taxable total:</source>
        <translation>Totale imponibile:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="129"/>
        <source>&amp;New</source>
        <translation>&amp;Nuovo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="131"/>
        <source>Create a new document</source>
        <translation>Crea una nuova distinta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="134"/>
        <source>&amp;Open...</source>
        <translation>&amp;Apri...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="136"/>
        <source>Open an existing document</source>
        <translation>Apri una distinta esistente</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="139"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="141"/>
        <source>Save the document to disk</source>
        <translation>Salva la distinta sul disco</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="144"/>
        <source>Save &amp;As...</source>
        <translation>Salva con &amp;nome...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="146"/>
        <source>Save the document under a new name</source>
        <translation>Salva la distinta con un nuovo nome</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="156"/>
        <source>&amp;Export document...</source>
        <translation>&amp;Esporta distinta...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="158"/>
        <source>Export document</source>
        <translation>Esporta la distinta per la stampa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="161"/>
        <source>E&amp;xit</source>
        <translation>E&amp;sci</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="163"/>
        <source>Quit the application</source>
        <translation>Esci dall&apos;applicazione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>&amp;About</source>
        <translation>&amp;Informazioni</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="167"/>
        <source>About this software</source>
        <translation>Informazioni su questo software</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="172"/>
        <source>Recent &amp;documents</source>
        <translation>&amp;Distinte recenti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="176"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="186"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="200"/>
        <source>Open document</source>
        <translation>Apri distinta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="202"/>
        <source>Reports (*.json);;                                                     All files (*.*)</source>
        <translation>Distinte (*.json);; Tutti i files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="208"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="209"/>
        <source>Document not valid.</source>
        <translation>Distinta non valida.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="226"/>
        <source>Save document</source>
        <translation>Salva la distinta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="228"/>
        <source>Reports (*.json)</source>
        <translation>Distinte (*.json)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>About</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="267"/>
        <source>Add row at top</source>
        <translation>Aggiungi una riga in cima</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="270"/>
        <source>Add row at bottom</source>
        <translation>Aggiungi una riga in fondo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="273"/>
        <source>Add row above</source>
        <translation>Aggiungi una riga sopra il cursore</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="276"/>
        <source>Add row below</source>
        <translation>Agiungi una riga sotto il cursore</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="283"/>
        <source>Remove selected row</source>
        <translation>Rimuovi riga selezionata</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Remove all rows</source>
        <translation>Rimuovi tutte le righe</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="356"/>
        <source>Document loaded: %1</source>
        <translation>Distinta caricata: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="363"/>
        <source>Document saved: %1</source>
        <translation>Distinta salvata: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="386"/>
        <source>&amp;%1 %2</source>
        <translation>&amp;%1 %2</translation>
    </message>
</context>
</TS>
